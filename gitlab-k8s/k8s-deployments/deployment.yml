apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: %SERVICE_NAME%
  namespace: %K8S_NAMESPACE%
spec:
  replicas: %K8S_REPLICAS%
  template:
    metadata:
      labels:
        app: %SERVICE_NAME%
        version: %VERSION%
        # environment: %CI_ENVIRONMENT_SLUG%
    spec:
      containers:
      # Main container for the API interface to the chaincode.
      - name: fchain-api
        image: %CI_REGISTRY_IMAGE%:%VERSION%.%SHORT_SHA% #latest # #
        imagePullPolicy: Always
        ports:
        #main service port
        - containerPort: %LISTENING_PORT%
          name: fchain-api-port
        env:
        - name: CHANNEL
          value: %FABRIC_CHANNEL%
        # Port is numeric, and its important it is in quotes. Otherwise k8s rejects it.
        - name: LISTENING_PORT
          value: "%LISTENING_PORT%"
        - name: IDENTITY
          value: %IDENTITY%
        - name: HFC_STORE_PATH
          value: %HFC_STORE_PATH%
        - name: PEERS
          value: %PEERS%
        - name: ORDERER
          value: %ORDERER%
        - name: EVENTHUB
          value: %EVENTHUB%
        # USE_TLS is boolean, and its important it is in quotes. Otherwise k8s rejects it.
        - name: USE_TLS
          value: "%FABRIC_USE_TLS%"

        readinessProbe:
          # Sometimes, applications are temporarily unable to serve traffic. For example, an application might need to
          # load large data or configuration files during startup. In such cases, you don’t want to kill the application,
          # but you don’t want to send it requests either. Kubernetes provides readiness probes to detect and mitigate
          # these situations. A pod with containers reporting that they are not ready does not receive traffic through
          # Kubernetes Services.
          httpGet:
            path: /health/readiness
            port: 3000
          periodSeconds: 20
          initialDelaySeconds: 60
          timeoutSeconds: 10
        livenessProbe:
          # Many applications running for long periods of time eventually transition to broken states, and cannot recover except by being restarted. Kubernetes provides liveness probes to detect and remedy such situations.
          # Liveness probe against the health actuator endpoint.  Pod restarted if fails...its hung.
          httpGet:
            path: /health/liveness
            port: 3000
          periodSeconds: 120
          initialDelaySeconds: 60
          timeoutSeconds: 10
        volumeMounts:
        - name: fabric-api-config-volume
          mountPath: /etc/hfc-key-store

        - name: crypto-config
          mountPath: /tmp/crypto
          subPath: crypto-config


        # Main container for the API interface to the chaincode.
      - name: fchain-ui
        image: %UI_REGISTRY_IMAGE%:latest
        imagePullPolicy: Always
        ports:
        # angular app is listening on this port.
        - containerPort: 4200
          name: angular-ui-port


        readinessProbe:
          # Sometimes, applications are temporarily unable to serve traffic. For example, an application might need to
          # load large data or configuration files during startup. In such cases, you don’t want to kill the application,
          # but you don’t want to send it requests either. Kubernetes provides readiness probes to detect and mitigate
          # these situations. A pod with containers reporting that they are not ready does not receive traffic through
          # Kubernetes Services.
          httpGet:
            path: /
            port: 4200
          periodSeconds: 20
          initialDelaySeconds: 60
          timeoutSeconds: 10
        livenessProbe:
          # Many applications running for long periods of time eventually transition to broken states, and cannot recover except by being restarted. Kubernetes provides liveness probes to detect and remedy such situations.
          # Liveness probe against the health actuator endpoint.  Pod restarted if fails...its hung.
          httpGet:
            path: /
            port: 4200
          periodSeconds: 120
          initialDelaySeconds: 60
          timeoutSeconds: 10


      #Telegraf sidecar container to forward JMX metrics to influxDB
      - name: telegraf
        image: telegraf:alpine
        imagePullPolicy: Always
        env:
       #Add metadata into environment visible to telegraf.toml for enhanced jmx reporting in grafana
        #- name: MY_CI_ENVIRONMENT
        #  value: %CI_ENVIRONMENT_SLUG%
        - name: MY_KUBE_DEPLOYMENT
          value: %SERVICE_NAME%
        - name: MY_KUBE_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        volumeMounts:
        - name: telegraf-config-volume
          mountPath: /etc/telegraf
      imagePullSecrets:
      - name: %CI_REGISTRY%
      volumes:
      #- name: fluentd-config-volume
      #  configMap:
      #    name: fluentd-config
      - name: telegraf-config-volume
        configMap:
          name: telegraf-config
      - name: fabric-api-config-volume
        configMap:
          name: fabric-api-cert-config-%SERVICE_NAME%
      - name: crypto-config
        persistentVolumeClaim:
          claimName: adapters