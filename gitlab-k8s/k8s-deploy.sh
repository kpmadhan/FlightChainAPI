#!/bin/bash
#
# Deploys the docker containers and configures the service and ingress definitions
#   Simple example of how to integrate K8s with gitlab-ci.
#   TODO: Will not support concurrent commits across teams. Use granular k8s namespaces to avoid collisions
#


set -e  # Exit if any command errors

echo "Environment Variable Sanity Check...."
if [ -z "$CI_PROJECT_NAME" ]; then echo "ERROR: CI_PROJECT_NAME is not set or is blank";  exit 1; else echo "CI_PROJECT_NAME is set to '$CI_PROJECT_NAME'"; fi
if [ -z "$CI_REGISTRY_IMAGE" ]; then echo "ERROR: CI_REGISTRY_IMAGE is not set or is blank";  exit 1; else echo "CI_REGISTRY_IMAGE is set to '$CI_REGISTRY_IMAGE'"; fi
if [ -z "$CI_REGISTRY" ]; then echo "ERROR: CI_REGISTRY is not set or is blank";  exit 1; else echo "CI_REGISTRY is set to '$CI_REGISTRY'"; fi
if [ -z "$SHORT_SHA" ]; then echo "ERROR: SHORT_SHA is not set or is blank";  exit 1; else echo "SHORT_SHA is set to '$SHORT_SHA'"; fi
if [ -z "$VERSION" ]; then echo "ERROR: VERSION is not set or is blank";  exit 1; else echo "VERSION is set to '$VERSION'"; fi
if [ -z "$K8S_NAMESPACE" ]; then echo "ERROR: K8S_NAMESPACE is not set or is blank";  exit 1; else echo "K8S_NAMESPACE is set to '$K8S_NAMESPACE'"; fi
if [ -z "$K8S_REPLICAS" ]; then echo "ERROR: K8S_REPLICAS is not set or is blank";  exit 1; else echo "K8S_REPLICAS is set to '$K8S_REPLICAS'"; fi
if [ -z "$LISTENING_PORT" ]; then echo "ERROR: LISTENING_PORT is not set or is blank";  exit 1; else echo "LISTENING_PORT is set to '$LISTENING_PORT'"; fi
if [ -z "$INGRESS_DOMAIN" ]; then echo "ERROR: INGRESS_DOMAIN is not set or is blank";  exit 1; else echo "INGRESS_DOMAIN is set to '$INGRESS_DOMAIN'"; fi
# Deployment domain is blank in PROD
#if [ -z "$DEPLOYMENT_DOMAIN" ]; then echo "ERROR: DEPLOYMENT_DOMAIN is not set or is blank";  exit 1; else echo "DEPLOYMENT_DOMAIN is set to '$DEPLOYMENT_DOMAIN'"; fi
if [ -z "$PORT_TYPE" ]; then echo "ERROR: PORT_TYPE is not set or is blank";  exit 1; else echo "PORT_TYPE is set to '$PORT_TYPE'"; fi
if [ -z "$SERVICE_NAME" ]; then echo "ERROR: SERVICE_NAME is not set or is blank";  exit 1; else echo "SERVICE_NAME is set to '$SERVICE_NAME'"; fi
if [ -z "$REGISTRY_TOKEN" ]; then echo "ERROR: REGISTRY_TOKEN is not set or is blank. This is for docker. Set it in https://gitlab.com/FlightChain2/FlightChainUI/settings/ci_cd ";  exit 1; else echo "REGISTRY_TOKEN is set to '$REGISTRY_TOKEN'"; fi
if [ -z "$UI_REGISTRY_IMAGE" ]; then echo "ERROR: UI_REGISTRY_IMAGE is not set or is blank. It should be set in gitlab-ci.yml ";  exit 1; else echo "UI_REGISTRY_IMAGE is set to '$UI_REGISTRY_IMAGE'"; fi

echo "Environment Variables required by nodejs app..."
if [ -z "$FABRICCHANNEL" ]; then echo "ERROR: FABRICCHANNEL is not set or is blank";  exit 1; else echo "FABRICCHANNEL is set to '$FABRICCHANNEL'"; fi
if [ -z "$FABRIC_USE_TLS" ]; then echo "ERROR: FABRIC_USE_TLS is not set or is blank";  exit 1; else echo "FABRIC_USE_TLS is set to '$FABRIC_USE_TLS'"; fi
if [ -z "$IDENTITY" ]; then echo "ERROR: IDENTITY is not set or is blank";  exit 1; else echo "IDENTITY is set to '$IDENTITY'"; fi
if [ -z "$HFC_STORE_PATH" ]; then echo "ERROR: HFC_STORE_PATH is not set or is blank";  exit 1; else echo "HFC_STORE_PATH is set to '$HFC_STORE_PATH'"; fi
if [ -z "$PEERS" ]; then echo "ERROR: PEERS is not set or is blank";  exit 1; else echo "PEERS is set to '$PEERS'"; fi
if [ -z "$ORDERER" ]; then echo "ERROR: ORDERER is not set or is blank";  exit 1; else echo "ORDERER is set to '$ORDERER'"; fi
if [ -z "$EVENTHUB" ]; then echo "ERROR: EVENTHUB is not set or is blank";  exit 1; else echo "EVENTHUB is set to '$EVENTHUB'"; fi
if [ -z "$CERT_DATA" ]; then echo "ERROR: CERT_DATA is not set or is blank";  exit 1; else echo "CERT_DATA is set to '$CERT_DATA'"; fi
if [ -z "$PUBLIC_KEY" ]; then echo "ERROR: PUBLIC_KEY is not set or is blank";  exit 1; else echo "PUBLIC_KEY is set to '$PUBLIC_KEY'"; fi
if [ -z "$PRIVATE_KEY" ]; then echo "ERROR: PRIVATE_KEY is not set or is blank";  exit 1; else echo "PRIVATE_KEY is set to '$PRIVATE_KEY'"; fi
if [ -z "$SIGNING_IDENTITY" ]; then echo "ERROR: SIGNING_IDENTITY is not set or is blank";  exit 1; else echo "SIGNING_IDENTITY is set to '$SIGNING_IDENTITY'"; fi

set -x  # For debugging purposes

# Replace tags in yaml files with pipeline variables
# - Keep in mind, sed expressions are sensitive to certain characters in the replacement strings
DEPLOYMENT_YMLS=k8s-deployments/*.yml

sed -i "s/%SERVICE_NAME%/$SERVICE_NAME/g" $DEPLOYMENT_YMLS
sed -i "s:%CI_REGISTRY_IMAGE%:$CI_REGISTRY_IMAGE:g" $DEPLOYMENT_YMLS  #Using colons to avoid forward slashes used in registry path
sed -i "s:%UI_REGISTRY_IMAGE%:$UI_REGISTRY_IMAGE:g" $DEPLOYMENT_YMLS  #Using colons to avoid forward slashes used in registry path
sed -i "s/%SHORT_SHA%/$SHORT_SHA/g" $DEPLOYMENT_YMLS
sed -i "s/%VERSION%/$VERSION/g" $DEPLOYMENT_YMLS
sed -i "s/%K8S_NAMESPACE%/$K8S_NAMESPACE/g" $DEPLOYMENT_YMLS
sed -i "s/%LISTENING_PORT%/$LISTENING_PORT/g" $DEPLOYMENT_YMLS
sed -i "s/%INGRESS_DOMAIN%/$INGRESS_DOMAIN/g" $DEPLOYMENT_YMLS
sed -i "s/%DEPLOYMENT_DOMAIN%/$DEPLOYMENT_DOMAIN/g" $DEPLOYMENT_YMLS
sed -i "s/%CI_REGISTRY%/$CI_REGISTRY/g" $DEPLOYMENT_YMLS
sed -i "s/%K8S_REPLICAS%/$K8S_REPLICAS/g" $DEPLOYMENT_YMLS
sed -i "s/%CI_ENVIRONMENT_SLUG%/$CI_ENVIRONMENT_SLUG/g" $DEPLOYMENT_YMLS
sed -i "s/%PORT_TYPE%/$PORT_TYPE/g" $DEPLOYMENT_YMLS
sed -i "s/%SERVICE_NAME%/$SERVICE_NAME/g" $DEPLOYMENT_YMLS

# Fabric API specific environment variables.
sed -i "s/%FABRIC_CHANNEL%/$FABRICCHANNEL/g" $DEPLOYMENT_YMLS
sed -i "s/%FABRIC_USE_TLS%/$FABRIC_USE_TLS/g" $DEPLOYMENT_YMLS
sed -i "s/%LISTEN_PORT%/$LISTEN_PORT/g" $DEPLOYMENT_YMLS
sed -i "s/%IDENTITY%/$IDENTITY/g" $DEPLOYMENT_YMLS
# Use pipe as delimiter because $HFC_STORE_PATH contains backslash
sed -i "s|%HFC_STORE_PATH%|$HFC_STORE_PATH|g" $DEPLOYMENT_YMLS
sed -i "s|%PEERS%|$PEERS|g" $DEPLOYMENT_YMLS
sed -i "s|%ORDERER%|$ORDERER|g" $DEPLOYMENT_YMLS
sed -i "s|%EVENTHUB%|$EVENTHUB|g" $DEPLOYMENT_YMLS

set +e  # Turn error checking off since next test will exit the program if namespace does not exist.
#cert for https of ingress web server
if ! kubectl get secret $SERVICE_NAME$DEPLOYMENT_DOMAIN-tls-secret -n $K8S_NAMESPACE
then
    openssl req -x509 -nodes -days 730 -newkey rsa:2048 -keyout tls.key -out tls.crt -subj "/CN=$SERVICE_NAME$DEPLOYMENT_DOMAIN.$INGRESS_DOMAIN"
    kubectl create secret tls $SERVICE_NAME$DEPLOYMENT_DOMAIN-tls-secret --key tls.key --cert tls.crt --namespace $K8S_NAMESPACE
fi

if ! kubectl get secret ui-ingress-auth -n $K8S_NAMESPACE
then
    # Create the auth secret that will be used in the ingress files.
    kubectl create secret generic ui-ingress-auth --from-file=auth --namespace $K8S_NAMESPACE
fi

set -e # Turn error checking back on

#Creates namespace or ignores if already there
## Using funky two stage create then apply throughout script to create if new and ignore or update if exists.
kubectl create namespace $K8S_NAMESPACE -o yaml --dry-run | kubectl apply -f -

#Needed so k8 can authenticate when pulling docker image of deployment.yml
# This might be better done as a one time activity to prevent dealing with tokena
kubectl create secret docker-registry $CI_REGISTRY --docker-server=$CI_REGISTRY --docker-username=$GITLAB_USER_EMAIL --docker-password=$REGISTRY_TOKEN --docker-email=$GITLAB_USER_EMAIL -o yaml --dry-run | kubectl apply --namespace=$K8S_NAMESPACE -f -


# Setup certificates for this airline
# These values are from the 'registerUser.js' script, and stored in the project runner settings as secrets
# We need to have a separate config map for each identity.
# This is referenced in deployment.yaml file.
mkdir hfc-key-store
echo $CERT_DATA > hfc-key-store/$IDENTITY
echo $PUBLIC_KEY > hfc-key-store/$SIGNING_IDENTITY-pub
echo $PRIVATE_KEY > hfc-key-store/$SIGNING_IDENTITY-priv
ls -al hfc-key-store
kubectl create configmap fabric-api-cert-config-$SERVICE_NAME --from-file=hfc-key-store -o yaml --dry-run | kubectl apply --namespace=$K8S_NAMESPACE -f -


# Setup telegraf configuration
mv monitoring/telegraf.toml telegraf.conf  #rename to match default config name
kubectl create configmap telegraf-config --from-file=telegraf.conf -o yaml --dry-run | kubectl apply --namespace=$K8S_NAMESPACE -f -



#For visual inspection of substitutions
cat k8s-deployments/*.yml

#apply all of the yamls in deployment folder
kubectl apply -f k8s-deployments --namespace=$K8S_NAMESPACE

