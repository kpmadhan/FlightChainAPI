import { EnvConfig } from './env';
import * as path from 'path';
import { ConfigOptions } from './config.model';
// import {LocalHFCAuthService} from "../../core/authentication/local-hfc-auth/local-authentication.service";
// import { PusherService } from '../../core/events/pusher/pusher.service';
// import { Auth0AuthenticationService } from '../../core/authentication/auth0/auth0-authentication.service';

export const Appconfig: ConfigOptions = {
    hlf: {
        walletPath: EnvConfig.HFC_STORE_PATH, //path.resolve(__dirname, `creds`),
        admin: {
            MspID: EnvConfig.MSPID
        },
        channelId: EnvConfig.CHANNEL,
        chaincodeId: 'flightchain',
        peerUrls: EnvConfig.PEERS,
        eventUrl: `${EnvConfig.EVENTHUB}`,
        ordererUrl: `${EnvConfig.ORDERER}`,

        tlsOptions: {
            trustedRoots: [],
            verify: false
        },
        caName: 'ca.sita.aero'
    }
} as ConfigOptions;

// export const EventService = {provide: 'IEventService', useClass: PusherService};
// export const AuthService = {provide: 'IAuthService', useClass: LocalHFCAuthService};
