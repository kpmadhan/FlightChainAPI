import {AcrisFlight} from "../../acris-schema/AcrisFlight";


export class ACRISFlightValidation {
    static generateUniqueKey(flight: AcrisFlight): string {
        let flightNum = flight.flightNumber.trackNumber;
        while (flightNum.length < 4)
            flightNum = '0' + flightNum;
        return flight.originDate + flight.departureAirport + flight.operatingAirline.iataCode + flightNum;
    }
}

