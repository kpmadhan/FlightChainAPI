import {Module, NestModule, RequestMethod} from '@nestjs/common';
import {RootModule} from "./root/root.module";
import {MiddlewaresConsumer} from "@nestjs/common/interfaces/middlewares";
import {FrontendMiddleware} from "./middleware/FrontendMiddleware";
import {MorganMiddleware} from "@nest-middlewares/morgan";
import {FlightChain2Module} from "./flight-chain2/flight-chain.module";
import {CoreModule} from "./core/core.module";
import {EnvConfig} from "./common/config/env";
import {Log} from "./common/utils/logging/log.service";
import {HealthModule} from "./health/health.module";



@Module({
  imports: [CoreModule, RootModule, FlightChain2Module, HealthModule],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {

    constructor() {
        EnvConfig.initialise();
    }

    configure(consumer: MiddlewaresConsumer): void {

        // IMPORTANT! Call Middleware.configure BEFORE using it for routes
        MorganMiddleware.configure( 'combined')
        consumer.apply(MorganMiddleware).forRoutes( {
            // @ts-ignore
            path: '/flightChain/*', method: RequestMethod.ALL
        });


        consumer.apply(FrontendMiddleware)
            .forRoutes({
                // @ts-ignore
                path: '*', method: RequestMethod.ALL
            },
        );
    }
}
