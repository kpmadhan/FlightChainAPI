import {
    Get,
    Controller,
    Param,
    HttpStatus,
    Post,
    Body,
    HttpException,
    Patch,
    Query,
    Inject,
    Res,
    Req,
    LoggerService, NotFoundException
} from '@nestjs/common';
import {AcrisFlight} from '../acris-schema/AcrisFlight';
import {
    ApiImplicitBody,
    ApiImplicitParam,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
    ApiImplicitQuery
} from '@nestjs/swagger';
import {FLIGHTCHAIN_ROUTE_PREFIX} from "../middleware/FrontendMiddleware";
import {FlightChain2Service} from "./flight-chain2.service";
import {FlightChainData} from "./FlightChainData";

@ApiUseTags('FlightChain2')
@Controller(FLIGHTCHAIN_ROUTE_PREFIX)
export class FlightChain2Controller {

    constructor(private flightChainService: FlightChain2Service) {
    }


    /**
     * @param channelName
     */
    @ApiOperation({
        title: 'Get chaincode version',
        description: 'Returns the version of the chaincode deployed',
    })
    @Get('/version')
    @ApiResponse({status: 200, description: 'Returns the version of the chaincode.', type: String})
    @ApiResponse({status: 404, description: 'Not flight matching the given flightKey has been found.'})
    public async getVersion(
        @Req() request): Promise<string> {
        return this.flightChainService.getVersion();
    }


    /**
     * Get live flight data from the blockchain
     * @param flightKey
     * @param channelName
     */
    @ApiOperation({
        title: 'Get one flight',
        description: 'Returns the live state of the flight identified by flightKey',
    })
    @ApiImplicitParam({
        name: 'flightKey',
        type: 'string',
        required: true,
        description: 'Unique key for each flight. The key is made up of [DepDate][DepAirport][OperatingAirline][OperatingFlightNum]. e.g. 2018-07-22LGWBA0227'
    })

    @Get('/:flightKey')
    @ApiResponse({status: 200, description: 'The flight has been successfully returned.', type: FlightChainData})
    @ApiResponse({status: 404, description: 'Not flight matching the given flightKey has been found.', type: NotFoundException})
    public async getOneFlight(
        @Param('flightKey') flightKey): Promise<FlightChainData> {
        // winstonLogger.debug(`FlightChainController.getOneFlight(channelName=${channelName}, flightKey=${flightKey})`);
        // winstonLogger.debug(request.url)
        // winstonLogger.debug(request.headers);
        return this.flightChainService.getFlight(flightKey);
    }


    /**
     * Create a new flight on the blockchain
     * @param flight
     * @param channelName
     */
    @ApiOperation({title: 'Create new flight on the network'})
    @Post()
    @ApiResponse({status: 200, description: 'The flight has been successfully created.', type:FlightChainData, isArray: false})
    @ApiResponse({status: 404, description: 'Not flight matching the given flightKey has been found.'})
    @ApiResponse({status: 400, description: 'The flight already exists, or the input flight data is invalid'})
    public async createFlight(
        @Body() flight: AcrisFlight): Promise<FlightChainData> {
        // winstonLogger.debug(`FlightChainController.createFlight(channelName=${channelName}), flightKey=${FlightChainService.generateUniqueKey(flight)}`);

        const flightCreated: FlightChainData = await this.flightChainService.createFlight(flight);
        if (flightCreated === undefined) {
            throw new HttpException('Invalid flight', HttpStatus.BAD_REQUEST);
        }
        return flightCreated;
    }

    /**
     * Update an existing flight on the network.
     * @param flightKey
     * @param flight
     * @param channelName
     */
    @ApiOperation({title: 'Update an existing flight on the network'})
    @ApiImplicitParam({
        name: 'flightKey',
        type: 'string',
        required: true,
        description: 'Unique key for each flight. The key is made up of [DepDate][DepAirport][OperatingAirline][OperatingFlightNum]. e.g. 2018-07-22LGWBA0227'
    })

    @Patch('/:flightKey')
    @ApiResponse({status: 200, description: 'The flight has been successfully updated.', type:FlightChainData, isArray: false})
    @ApiResponse({status: 404, description: 'Not flight matching the given flightKey has been found.'})
    @ApiResponse({status: 400, description: 'The input flight data is invalid'})
    public async updateFlight(
        @Param('flightKey') flightKey, @Body() flight: AcrisFlight): Promise<FlightChainData> {
        // winstonLogger.debug(`FlightChainController.updateFlight(channelName=${channelName}, flightKey=${flightKey})`);
        return this.flightChainService.updateFlight(flightKey, flight);
    }


    /**
     *
     * Get the history of all updates to a flight from the blockchain.
     *
     * @param flightKey
     * @param channelName
     */
    @ApiOperation({
        title: 'Get flight history',
        description: 'Returns the history of udpates for the flight identified by flightKey'
    })
    @ApiImplicitParam({
        name: 'flightKey',
        type: 'string',
        required: true,
        description: 'Unique key for each flight. The key is made up of [DepDate][DepAirport][OperatingAirline][OperatingFlightNum]. e.g. 2018-07-22LGWBA0227'
    })

    @Get('/:flightKey/history')
    @ApiResponse({status: 200, description: 'The flight has been successfully returned.', type:FlightChainData, isArray: true})
    @ApiResponse({status: 404, description: 'Not flight matching the given flightKey has been found.'})
    public async getFlightHistory(
        @Param('flightKey') flightKey): Promise<FlightChainData[]> {
        // winstonLogger.debug(`FlightChainController.getFlightHIstory(channelName=${channelName}, flightKey=${flightKey})`);
        return this.flightChainService.getFlightHistory(flightKey);
    }


    @ApiOperation({title: 'Get transaction info', description: 'Returns the details of a given transaction'})
    @ApiImplicitParam({
        name: 'transactionId',
        type: 'string',
        required: true,
        description: 'Transaction Id returned after every flight creation or update.'
    })
    @Get('/transaction/:transactionId')
    @ApiResponse({status: 200, description: 'The transaction info has been successfully returned.'})
    @ApiResponse({status: 404, description: 'Not transaction info matching the given transactionId has been found.'})
    @ApiResponse({status: 500, description: 'Unknown internal server error.'})
    public async getTransactionInfo(
        @Param('transactionId') transactionId): Promise<any> {
        // winstonLogger.debug(`FlightChainController.getTransactionInfo(channelName=${channelName}, transactionId=${transactionId})`);
        return this.flightChainService.getTransactionInfo(transactionId);
    }




}
